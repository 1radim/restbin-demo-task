#!/usr/bin/env python3
import os
import stat
import sys
from datetime import datetime, date, time, timezone, timedelta

from dateutil import tz

"""
Go counts from 0001-01-01 00:00:00 UTC, Unix counts from 1970-01-01 00:00:00 UTC.
"""
SECONDS_PER_DAY = 86400  # 24 * 60 * 60
GO_MAGIC_UNIX_TO_INTERNAL = int(1969 * 365 + 1969 / 4 - 1969 / 100 + 1969 / 400) * SECONDS_PER_DAY
GO_MAGIC_INTERNAL_TO_UNIX = -GO_MAGIC_UNIX_TO_INTERNAL


def main():
    stdin_fd = sys.stdin.fileno()
    stdin_mode = os.fstat(stdin_fd).st_mode
    if stat.S_ISFIFO(stdin_mode):
        data: bytes = sys.stdin.buffer.read()
        dt = unmarshal_time(data)
        print(rfc3339_format(dt), end='')
    else:
        dt = datetime.combine(
            date(2023, 12, 31),
            time(12, 42, 59, 987654),
            tz.tzlocal()
        )
        data = marshal_time(dt)
        sys.stdout.buffer.write(data)


def marshal_time(dt: datetime) -> bytes:
    """
    marshal_time is Python implementation of Go's (time.Time).MarshalBinary
    """
    offset_min: int = 0
    offset_sec: int = 0
    version = 1

    if dt.tzname() == "UTC":
        offset_min = -1
    else:
        offset: datetime.timedelta = dt.utcoffset()
        if offset.seconds % 60 != 0:
            version = 2
            offset_sec = offset.seconds % 60

        offset_min = offset.seconds // 60
        if offset_min < -32768 or offset_min == -1 or offset_min > 32767:
            raise Exception("unexpected zone offset")

    unix_sec = int(dt.timestamp())
    go_sec = unix_sec + GO_MAGIC_UNIX_TO_INTERNAL
    nsec = dt.microsecond * int(1e3)
    enc = bytes([
        version,  # byte 0 : version
        (go_sec >> 56) % 256,  # bytes 1-8: seconds
        (go_sec >> 48) % 256,
        (go_sec >> 40) % 256,
        (go_sec >> 32) % 256,
        (go_sec >> 24) % 256,
        (go_sec >> 16) % 256,
        (go_sec >> 8) % 256,
        (go_sec >> 0) % 256,
        (nsec >> 24) % 256,  # bytes 9-12: nanoseconds
        (nsec >> 16) % 256,
        (nsec >> 8) % 256,
        (nsec >> 0) % 256,
        (offset_min >> 8) % 256,  # bytes 13-14: zone offset in minutes
        (offset_min >> 0) % 256,
    ])
    if version == 2:
        enc += bytes([
            offset_sec % 256,  # byte 15: zone offset in seconds
        ])
    return enc


def unmarshal_time(data: bytes) -> datetime:
    """
    unmarshal_time is Python implementation of Go's (time.Time).UnmarshalBinary
    """
    if len(data) == 0:
        raise Exception("no data")

    version = data[0]
    if version not in [1, 2]:
        raise Exception("unsupported version")

    # version + sec + nsec + zone offset
    want_len = 1 + 8 + 4 + 2
    if version == 2:
        want_len += 1
    if len(data) is not want_len:
        raise Exception("invalid length")

    buf = data[1:]
    sec = int(buf[7]) | int(buf[6]) << 8 | int(buf[5]) << 16 | int(buf[4]) << 24 | int(buf[3]) << 32 | int(
        buf[2]) << 40 | int(buf[1]) << 48 | int(buf[0]) << 56
    buf = buf[8:]
    nsec = int(buf[3]) | int(buf[2]) << 8 | int(buf[1]) << 16 | int(buf[0]) << 24

    buf = buf[4:]
    offset_min = ((int(buf[1]) % (2 ** 16)) | (int(buf[0]) % (2 ** 16)) << 8)
    offset_sec = offset_min * 60
    if version == 2:
        offset_sec += int(buf[2])

    dt: datetime = datetime.fromtimestamp((sec + GO_MAGIC_INTERNAL_TO_UNIX) + (nsec / 1e9), tz=timezone.utc)

    if offset_sec == -1 * 60:
        dt = dt.replace(tzinfo=timezone.utc)
    elif tz.tzlocal().utcoffset(dt).seconds == offset_sec:
        dt = dt.replace(tzinfo=tz.tzlocal())
    else:
        dt = dt.replace(tzinfo=timezone(timedelta(seconds=offset_sec)))

    return dt


def rfc3339_format(dt: datetime) -> str:
    return dt.isoformat().replace('+00:00', 'Z')


if __name__ == '__main__':
    main()
