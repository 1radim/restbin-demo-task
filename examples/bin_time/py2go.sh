#!/bin/sh

script_dir=$(dirname -- "$( readlink -f -- "$0"; )")


python3 "$script_dir/main.py" | go run "$script_dir/main.go"